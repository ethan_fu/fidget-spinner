* How to run
Open idle
Open fidget_spinner.py in idle
Make sure fidget_spinner.py is saved
Press Ctrl+F5

* How to use Git
** open up cygwin

cd /cygdrive/c/Users/PeiYing/Desktop/ethans-coding-things/snake
git status
# find out which file has changes, press 'q' if stuck
git diff [filename]
# see the changes in a specific file, or all files if no file specified
git add file
git commit --message "new message"
git push



then edit the files again

# press 'q' if stuck
git diff
git add Ethan<TAB>
git commit --message "write a message in imperative voice"

to see all commits type <git log --oneline>
to get more info if dont type <--oneline>
