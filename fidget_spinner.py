#Todo list
'''
In future, make better solution for keeping track of click counter

massage code-
    make code easier to read for new person
    combine duplicated concepts for handeling two different buttons into a class
    use a single global universe object
        use less globals and more functions

Make code more pure - pure functions
'''
#center any fidgetspinner- find new, or try to do math/play around
#    with the picture to find center
# add to respective dictionaries:
#                           toggle last click frame and speed last click frame
import pygame
pygame.init()

window_width = 600
window_height = 600

color = pygame.colordict.THECOLORS

screen = pygame.display.set_mode([window_width, window_height])

running = True

spinner = pygame.image.load('fidget_spinner.png')
spinner_width, spinner_height = spinner.get_size()
spinner_x_center_offset = -1*2
spinner_y_center_offset = 19*2
spinner_width += spinner_x_center_offset
spinner_height += spinner_y_center_offset

spinning_angle = 0
centered_speed_text = (0,0)
centered_toggle_text = (0,0)
font = pygame.font.SysFont("comicsansms", 21)
toggle_text = font.render('Start/Stop', True, color['black'])
speed_text = font.render('Go speedy', True, color['black'])
toggle_width, toggle_height = font.size("Start/Stop")
speed_width, speed_height = font.size("Go speedy")
last_ticks = pygame.time.get_ticks()
mouse_was_pressed = False
mouse_is_pressed = False
frame_number = 0
box_width = 120
box_height = 80

#https://mike632t.wordpress.com/2018/02/10/displaying-a-list-of-the-named-colours-available-in-pygame/

def blit_rotate(surf, image, pos, originPos, angle):
    #Borrowed:
    #https://stackoverflow.com/questions/4183208/how-do-i-rotate-an-image-around-its-center-using-pygame
    # calculate the axis aligned bounding box of the rotated image
    w, h       = image.get_size()
    box        = [pygame.math.Vector2(p)
                  for p in [(0, 0), (w, 0), (w, -h), (0, -h)]]
    box_rotate = [p.rotate(angle) for p in box]
    min_box    = (min(box_rotate, key=lambda p: p[0])[0],
                  min(box_rotate, key=lambda p: p[1])[1])
    max_box    = (max(box_rotate, key=lambda p: p[0])[0],
                  max(box_rotate, key=lambda p: p[1])[1])

    # calculate the translation of the pivot
    pivot        = pygame.math.Vector2(originPos[0], -originPos[1])
    pivot_rotate = pivot.rotate(angle)
    pivot_move   = pivot_rotate - pivot

    # calculate the upper left origin of the rotated image
    #HOW

    origin = (int(pos[0] - originPos[0] + min_box[0] - pivot_move[0]),
              int(pos[1] - originPos[1] - max_box[1] + pivot_move[1]))

    # get a rotated image
    rotated_image = pygame.transform.rotate(image, angle)

    # rotate and blit the image
    surf.blit(rotated_image, origin)

def mouse_in_box(x, y, w, h, mouse):
    return x < mouse[0] < x + w and y < mouse[1] < y + h

def button_clicked(x, y,
                   w, h,
                   mouse, mouse_was_pressed, mouse_is_pressed,
                   button_action, frame_number,
                   button):

    if(mouse_in_box(x, y, w, h, mouse)
       and (not mouse_was_pressed)
       and mouse_is_pressed):
        button_action(frame_number, button)

        return True
    else:
        return False

speed_button = {"last_click_frame": -1,
                 "click_count": 0}

def speed_button_clicked(x, y, w, h, mouse):
    global mouse_was_pressed, mouse_is_pressed, frame_number
    global speed_button
    def speed_button_action(frame, speed_button):
        if speed_button["last_click_frame"] is not frame:
            speed_button["click_count"] += 1
            speed_button["last_click_frame"] = frame
    button_clicked(x, y,
                   w, h,
                   mouse, mouse_was_pressed, mouse_is_pressed,
                   speed_button_action, frame_number,
                   speed_button)

toggle_button = {"last_click_frame": -1,
                 "click_count": 0}

def toggle_button_clicked(x, y, w, h, mouse):
    global mouse_was_pressed, mouse_is_pressed, frame_number
    global toggle_button
    def toggle_button_action(frame, toggle_button):
        if toggle_button["last_click_frame"] is not frame:
            toggle_button["click_count"] += 1
            toggle_button["last_click_frame"] = frame
    button_clicked(x, y,
                   w, h,
                   mouse, mouse_was_pressed, mouse_is_pressed,
                   toggle_button_action, frame_number,
                   toggle_button)

def center_label(x, y, button_width, button_height,
                 text_width, text_height):
    return (x + (button_width - text_width)/2,
            y + (button_height - text_height)/2)

def do_speed_button(x, y, w, h, ic, ac):
    global centered_speed_text

    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()
    pygame.draw.rect(screen, ic, (x, y, w, h))

    speed_button_clicked(x, y, w, h, mouse)

    if mouse_in_box(x, y, w, h, mouse):
        pygame.draw.rect(screen, ac, (x, y, w, h))

    centered_speed_text = center_label(x, y, box_width, box_height,
                                       speed_width, speed_height)
    screen.blit(speed_text, centered_speed_text)

def do_toggle_button(x, y, w, h, ic, ac):
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()
    pygame.draw.rect(screen, ic, (x, y, w, h))

    toggle_button_clicked(x, y, w, h, mouse)

    if mouse_in_box(x, y, w, h, mouse):
        pygame.draw.rect(screen, ac, (x, y, w, h))

    centered_toggle_text = center_label(x, y, box_width, box_height,
                                        toggle_width, toggle_height)

    screen.blit(toggle_text, centered_toggle_text)

while running:

    button_states = pygame.mouse.get_pressed()
    mouse_is_pressed = button_states[0]

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    screen.fill((255, 255, 255))

    do_toggle_button(350, 500,
                     box_width, box_height,
                     color['cornflowerblue'], color['darkblue'])

    do_speed_button(130, 500,
                    box_width, box_height,
                    color['cornflowerblue'], color['darkblue'])

    pygame.draw.circle(screen, (0,0,255),
                       (int(window_width/2), int(window_height/2)),
                       int(56/2))

    if (toggle_button["click_count"] % 2) != 0:
        tick_delta = (pygame.time.get_ticks()- last_ticks)
        spinning_angle += tick_delta/5 * (1 + speed_button["click_count"])

    blit_rotate(screen, spinner,
                (window_width/2, window_height/2),
                (spinner_width/2, spinner_height/2),
                spinning_angle)

    mouse_was_pressed = mouse_is_pressed

    frame_number += 1

    last_ticks = pygame.time.get_ticks()

    pygame.display.flip()

pygame.quit()
